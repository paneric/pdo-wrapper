<?php

namespace spec\Paneric\PdoWrapper;

use Paneric\PdoWrapper\DataPreparator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DataPreparatorSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(DataPreparator::class);
    }

    public function it_prepares_insert_data_sets(): void
    {
        $this->prepareInsertDataSets([
            ['id' => 1, 'ref' => 'ref_1'],
            ['id' => 2, 'ref' => 'ref_2'],
            ['id' => 3, 'ref' => 'ref_3'],
        ])->shouldReturn([
            ['id' => 1, 'ref' => "ref_1"],
            ['id1' => 2, 'ref1' => "ref_2"],
            ['id2' => 3, 'ref2' => "ref_3"],
        ]);
    }

    public function it_prepares_where_in_data_set(): void
    {
        $this->prepareWhereInDataSet(
            'id',
            [1, 2, 3]
        )->shouldReturn([
            'id' => 1,
            'id1' => 2,
            'id2' => 3
        ]);
    }

    public function it_prepares_where_multiple_data_set(): void
    {
        $this->prepareWhereMultipleDataSet([
            'id' => [1, 2, 3],
            'ref' => ['user1', 'user2', 'user3']
        ])->shouldReturn([
            'id' => ['id' => 1, 'id1' => 2, 'id2' => 3],
            'ref' => ['ref' => 'user1', 'ref1' => 'user2', 'ref2' => 'user3']
        ]);
    }

    public function it_chains_data_sets(): void
    {
        $this->chainDataSets([
            ['id' => 1, 'ref' => "ref_1"],
            ['id1' => 2, 'ref1' => "ref_2"],
            ['id2' => 3, 'ref2' => "ref_3"],
        ])->shouldReturn([
            'id' => 1, 'ref' => "ref_1", 'id1' => 2, 'ref1' => "ref_2", 'id2' => 3, 'ref2' => "ref_3"
        ]);
    }
}
