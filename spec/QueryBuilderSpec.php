<?php

declare(strict_types=1);

namespace spec\Paneric\PdoWrapper;

use Paneric\PdoWrapper\QueryBuilder;
use Paneric\PdoWrapper\SequencePreparator;
use PhpSpec\ObjectBehavior;

class QueryBuilderSpec extends ObjectBehavior
{
    public function let(SequencePreparator $sequencePreparator): void
    {
        $this->beConstructedWith($sequencePreparator);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(QueryBuilder::class);
        $this->shouldBeAnInstanceOf(QueryBuilder::class);
        $this->shouldImplement(QueryBuilder::class);
    }

    public function it_sets_select_default()
    {
        $this->select('user');
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user'
        );
    }

    public function it_sets_select_custom()
    {
        $this->select('user', ['id', 'ref']);
        $this->getQuery()->shouldReturn(
            'SELECT id, ref FROM user'
        );
    }

    public function it_sets_select_where_with_operator()
    {
        $this->select('user');
        $this->where(['id' => 1, 'ref' => 'user1'], 'AND');
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE (id=:id AND ref=:ref)'
        );
    }

    public function it_sets_select_where_without_operator()
    {
        $this->select('user');
        $this->where(['id' => 1, 'ref' => 'user1']);
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE (id=:id AND ref=:ref)'
        );
    }

    public function it_sets_where_in_without_operator()
    {
        $this->select('user');
        $this->whereIn(
            ['id1' => 'val1', 'id2' => 'val2'],
            'id'
        );
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE id IN (:id1, :id2)'
        );
    }

    public function it_sets_where_in_with_operator()
    {
        $this->select('user');
        $this->whereIn(
            ['id1' => 'val1', 'id2' => 'val2'],
            'id',
            '',
            'AND'
        );
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user AND id IN (:id1, :id2)'
        );
    }

    public function it_sets_where_not_in_without_operator()
    {
        $this->select('user');
        $this->whereIn(
            ['id1' => 'val1', 'id2' => 'val2'],
            'id',
            'NOT'
        );
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE id NOT IN (:id1, :id2)'
        );
    }

    public function it_sets_where_not_in_with_operator()
    {
        $this->select('user');
        $this->whereIn(
            ['id1' => 'val1', 'id2' => 'val2'],
            'id',
            'NOT',
            'and'
        );
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user AND id NOT IN (:id1, :id2)'
        );
    }

    public function it_sets_select_where_same()
    {
        $this->select('user');
        $this->whereSame([
            'id' => ['id' => 1, 'id1' => 2],
            'ref' => ['ref' => 'user1', 'ref1' => 'user2']
        ]);
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE id=:id OR id=:id1 OR ref=:ref OR ref=:ref1'
        );
    }

    public function it_sets_select_where_like_1()
    {
        $this->select('user');
        $this->whereLike([
            'id' => ['id' => 1, 'id1' => 2],
            'ref' => ['ref' => 'user1', 'ref1' => 'user2']
        ], [
            'OR', 'AND', 'OR'
        ]);
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE (id LIKE :id OR id LIKE :id1) AND (ref LIKE :ref OR ref LIKE :ref1)'
        );
    }

    public function it_sets_select_where_like_2()
    {
        $this->select('user');
        $this->whereLike([
            'id' => ['id' => 1, 'id1' => 2]
        ], [
            'OR'
        ]);
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE (id LIKE :id OR id LIKE :id1)'
        );
    }

    public function it_sets_select_where_like_3()
    {
        $this->select('user');
        $this->whereLike([
            'id' => ['id' => 1]
        ]);
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE (id LIKE :id)'
        );
    }

    public function it_sets_select_where_order_by()
    {
        $this->select('user');
        $this->where(['id' => 1, 'ref' => 'user1']);
        $this->orderBy(['ref' => 'ASC', 'age' => 'DESC']);
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE (id=:id AND ref=:ref) ORDER BY ref ASC, age DESC'
        );
    }

    public function it_sets_select_where_order_by_limit_offset()
    {
        $this->select('user');
        $this->where(['id' => 1, 'ref' => 'user1']);
        $this->orderBy(['ref' => 'ASC', 'age' => 'DESC']);
        $this->limit();
        $this->offset();
        $this->getQuery()->shouldReturn(
            'SELECT * FROM user WHERE (id=:id AND ref=:ref) ORDER BY ref ASC, age DESC LIMIT :limit OFFSET :offset'
        );
    }

    public function it_sets_insert(SequencePreparator $sequencePreparator)
    {
        $sequencePreparator->prepareInsertSequences(['ref' => 'user1', 'age' => 21])
            ->willReturn([
                'names'  => '(ref, age)',
                'values' => '(:ref, :age)'
            ]);

        $this->insert('user', ['ref' => 'user1', 'age' => 21]);
        $this->getQuery()->shouldReturn(
            'INSERT INTO user (ref, age) VALUES (:ref, :age)'
        );
    }

    public function it_sets_insert_multiple(SequencePreparator $sequencePreparator)
    {
        $sequencePreparator->prepareInsertMultipleSequences([
            ['ref' => 'user1', 'age' => 21],
            ['ref1' => 'user2', 'age1' => 22]
        ])->willReturn([
            'names'  => '(ref, age)',
            'values' => '(:ref, :age), (:ref1, :age1)'
        ]);

        $this->insertMultiple('user', [['ref' => 'user1', 'age' => 21], ['ref1' => 'user2', 'age1' => 22]]);
        $this->getQuery()->shouldReturn(
            'INSERT INTO user (ref, age) VALUES (:ref, :age), (:ref1, :age1)'
        );
    }

    public function it_sets_update(SequencePreparator $sequencePreparator)
    {
        $sequencePreparator->prepareUpdateSequence(['ref' => 'user1', 'age' => 21])
            ->willReturn('ref=:ref, age=:age');

        $this->update('user', ['ref' => 'user1', 'age' => 21]);
        $this->where(['id' => 'id1', 'ref' => 'user2']);
        $this->getQuery()->shouldReturn(
            'UPDATE user SET ref=:ref, age=:age WHERE (id=:id AND ref=:ref)'
        );
    }

    public function it_sets_update_multiple(SequencePreparator $sequencePreparator)
    {
        $sequencePreparator->prepareInsertMultipleSequences([
            ['id' => 1, 'ref' => 'ref', 'age' => 21],
            ['id1' => 2, 'ref1' => 'ref1', 'age1' => 22],
            ['id2' => 3, 'ref2' => 'ref2', 'age2' => 23],
        ])->willReturn([
            'names' => '(id, ref, age)',
            'values' => '(:id, :ref, :age), (:id1, :ref1, :age1), (:id2, :ref2, :age2)'
        ]);

        $sequencePreparator->prepareOnDuplicateSequences(
            ['id' => 1, 'ref' => 'ref', 'age' => 21], 'id'
        )->willReturn('ON DUPLICATE KEY UPDATE ref=VALUES(ref), age=VALUES(age)');

        $this->updateMultiple(
            'user',
            [
                ['id' => 1, 'ref' => 'ref', 'age' => 21],
                ['id1' => 2, 'ref1' => 'ref1', 'age1' => 22],
                ['id2' => 3, 'ref2' => 'ref2', 'age2' => 23],
            ]
        );
        $this->getQuery()->shouldReturn(
            'INSERT INTO user (id, ref, age) VALUES (:id, :ref, :age), (:id1, :ref1, :age1), (:id2, :ref2, :age2) ON DUPLICATE KEY UPDATE ref=VALUES(ref), age=VALUES(age)'
        );
    }

    public function it_sets_update_same(SequencePreparator $sequencePreparator): void
    {
        $sequencePreparator->prepareUpdateSequence(['age' => 22])
            ->willReturn('age=:age');

        $this->updateSame('user', ['age' => 22], ['id' => 1, 'id1' => 2, 'id2' => 3], 'id');
        $this->getQuery()->shouldReturn(
            'UPDATE user SET age=:age WHERE id IN (:id, :id1, :id2)'
        );
    }

    public function it_sets_delete()
    {
        $this->delete('user', ['ref' => 'user1', 'age' => 21]);
        $this->getQuery()->shouldReturn(
            'DELETE FROM user WHERE (ref=:ref AND age=:age)'
        );
    }

    public function it_sets_delete_multiple()
    {
        $this->deleteMultiple('user', ['id' => 1, 'id1' => 2, 'id2' => 3]);
        $this->getQuery()->shouldReturn(
            'DELETE FROM user WHERE id IN (:id, :id1, :id2)'
        );
    }
}

