<?php

declare(strict_types=1);

namespace spec\Paneric\PdoWrapper;

use Paneric\PdoWrapper\SequencePreparator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SequencePreparatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SequencePreparator::class);
    }

    public function it_prepares_insert_sequences(): void
    {
        $this->prepareInsertSequences(['ref' => 'user1', 'age' => 21])
            ->shouldReturn([
                'names'  => '(ref, age)',
                'values' => '(:ref, :age)'
            ]);
    }

    public function it_prepares_insert_multiple_sequences(): void
    {
        $this->prepareInsertMultipleSequences([
            ['ref' => 'user1', 'age' => 21],
            ['ref1' => 'user2', 'age1' => 22]
        ])->shouldReturn([
            'names'  => '(ref, age)',
            'values' => '(:ref, :age), (:ref1, :age1)'
        ]);
    }

    public function it_prepares_update_sequence(): void
    {
        $this->prepareUpdateSequence(['ref' => 'user1', 'age' => 21])
            ->shouldReturn('ref=:ref, age=:age');
    }

    public function it_prepares_on_duplicate_sequences(): void
    {
        $this->prepareOnDuplicateSequences(
            ['id' => 1, 'ref' => 'ref', 'age' => 21]
        )->shouldReturn('ON DUPLICATE KEY UPDATE ref=VALUES(ref), age=VALUES(age)');
    }
}
