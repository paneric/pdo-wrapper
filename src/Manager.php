<?php

declare(strict_types=1);

namespace Paneric\PdoWrapper;

use Exception;
use PDO;
use PDOStatement;

class Manager
{
    private $pdo;
    private $queryBuilder;
    private $dataPreparator;
    private $table;
    private $fetchMode;
    private $dtoClass;
    private $transactionInProgress = 0;

    public function __construct(PDO $pdo, QueryBuilder $queryBuilder, DataPreparator $dataPreparator)
    {
        $this->pdo = $pdo;
        $this->queryBuilder = $queryBuilder;
        $this->dataPreparator = $dataPreparator;
    }


    public function getPdo(): PDO
    {
        return $this->pdo;
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return $this->queryBuilder;
    }


    public function setTable(string $table): self
    {
        $this->table = $table;

        return $this;
    }

    public function setDTOClass(string $dtoClass): self
    {
        $this->dtoClass = $dtoClass;

        return $this;
    }

    public function setFetchMode(int $fetchMode): self
    {
        $this->fetchMode = $fetchMode;

        return $this;
    }


    public function findOneBy(array $criteria, string $operator = 'AND')//: false|[]|Object
    {
        $this->queryBuilder
            ->select($this->table)
            ->where($criteria, $operator);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteria
        );

        return $stmt->fetch();
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null)// false|[[]]|Object[]
    {
        $this->queryBuilder->select($this->table);

        if (!empty($criteria)) {
            $this->queryBuilder->where($criteria);
        }

        if ($orderBy !== null) {
            $this->queryBuilder->orderBy($orderBy);
        }

        if ($limit !== null) {
            $this->queryBuilder->limit();
            $criteria['limit'] = $limit;
        }

        if ($offset !== null) {
            $this->queryBuilder->offset();
            $criteria['offset'] = $offset;
        }

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteria
        );

        return $stmt->fetchAll();
    }


    public function findBySame(array $criteriaSame, string $operator = 'OR')// false|array
    {
        $criteriaSame = $this->dataPreparator->prepareWhereMultipleDataSet($criteriaSame);

        $this->queryBuilder
            ->select($this->table)
            ->whereSame($criteriaSame, $operator);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs($criteriaSame)
        );

        return $stmt->fetchAll();
    }

    public function findByLike(array $criteriaLike, array $operators = ['OR'])// false|array
    {
        $criteriaLike = $this->dataPreparator->prepareWhereMultipleDataSet($criteriaLike);

        $this->queryBuilder
            ->select($this->table)
            ->whereLike($criteriaLike, $operators);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs($criteriaLike)
        );

        return $stmt->fetchAll();
    }


    public function create(array $fieldsSet): string
    {
        $this->queryBuilder->insert($this->table, $fieldsSet);

        $this->setStmt(
            $this->queryBuilder->getQuery(),
            $fieldsSet
        );

        return $this->pdo->lastInsertId();
    }

    public function createMultiple(array $fieldsSets): string
    {
        $fieldsSets = $this->dataPreparator->prepareInsertDataSets($fieldsSets);

        $this->queryBuilder->insertMultiple($this->table, $fieldsSets);

        $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->dataPreparator->chainDataSets($fieldsSets)
        );

        return $this->pdo->lastInsertId();
    }

    public function update(array $fields, array $criteria): int
    {
        $this->queryBuilder->update($this->table, $fields)
            ->where($criteria, 'AND');

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs([$fields, $criteria])
        );

        return $stmt->rowCount();
    }
    /**
     * ALL FIELDS HAS TO BE MENTIONED !!!
     * If the new row is inserted, the number of affected-rows is 1.
     * If the existing row is updated, the number of affected-rows is 2.
     * If the existing row is updated using its current values, the number of affected-rows is 0.
     */
    public function updateMultiple(array $fieldsSets, string $idKey = 'id'): string
    {
        $fieldsSets = $this->dataPreparator->prepareInsertDataSets($fieldsSets);

        $this->queryBuilder->updateMultiple($this->table, $fieldsSets, $idKey);

        $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->dataPreparator->chainDataSets($fieldsSets)
        );

        return $this->pdo->lastInsertId();
    }

    public function updateSame(array $fields, array $values, string $field = 'id'): int
    {
        $criteriaInSet = $this->dataPreparator->prepareWhereInDataSet($field, $values);

        $this->queryBuilder->updateSame($this->table, $fields, $criteriaInSet, $field);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs([$fields, $criteriaInSet])
        );

        return $stmt->rowCount();
    }

    public function delete(array $criteria): int
    {
        $this->queryBuilder->delete($this->table, $criteria);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteria
        );

        return $stmt->rowCount();
    }

    public function deleteMultiple(array $values, string $field = 'id'): int
    {
        $criteriaInSet = $this->dataPreparator->prepareWhereInDataSet($field, $values);

        $this->queryBuilder->deleteMultiple($this->table, $criteriaInSet, $field);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteriaInSet
        );

        return $stmt->rowCount();
    }


    public function setStmt(string $query, array $args = null): PDOStatement
    {
        if ($args === null) {

            $stmt = $this->pdo->query($query);

            try {
                if ($stmt === false) {
                    if ($this->transactionInProgress === 1) {
                        $this->rollBack();
                    }

                    throw new Exception(__CLASS__ . ': PDO::query exception.');
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            return $this->setStmtFetchMode($stmt);
        }

        $stmt = $this->pdo->prepare($query);

        try {
            if ($stmt->execute($args) === false) {
                if ($this->transactionInProgress === 1) {
                    $this->rollBack();
                }

                throw new Exception(__CLASS__ . ': PDOStatement::execute exception.');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $this->setStmtFetchMode($stmt);
    }


    public function query(string $query): PDOStatement
    {
        return $this->pdo->query($query);
    }

    public function beginTransaction(): void
    {
        $this->transactionInProgress = 1;
        $this->pdo->beginTransaction();
    }

    public function commit(): void
    {
        $this->transactionInProgress = 0;
        $this->pdo->commit();
    }

    public function rollBack(): void
    {
        $this->transactionInProgress = 0;
        $this->pdo->rollBack();
    }

    public function getLastInsertId(): String
    {
        return $this->pdo->lastInsertId();
    }


    private function mergeArgs(array $valuesSets): array
    {
        $args = [];

        foreach ($valuesSets as $values) {
            $args += $values;
        }

        return $args;
    }

    private function setStmtFetchMode(PDOStatement $stmt): ?PDOStatement
    {
        if ($this->fetchMode === PDO::FETCH_CLASS) {
            $stmt->setFetchMode(PDO::FETCH_CLASS, $this->dtoClass);
            return $stmt;
        }

        if ($this->fetchMode === PDO::FETCH_ASSOC) {
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt;
        }

        $stmt->setFetchMode((PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE), $this->dtoClass);

        return $stmt;
    }
}
