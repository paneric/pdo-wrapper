<?php

declare(strict_types=1);

namespace Paneric\PdoWrapper;

use Exception;
use Paneric\Interfaces\Hydrator\HydratorInterface;
use Paneric\Interfaces\PDORepository\PDORepositoryInterface;

class PDORepository extends Repository implements PDORepositoryInterface
{
    public function findOneById(int $id): ?object
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['id' => $id]);

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['id' => $id]
        );

        $field = $stmt->fetch();

        if ($field === false){

            return null;
        }

        return $field;
    }

    public function findOneByRef(string $ref): ?object
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['ref' => $ref]);

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['ref' => $ref]
        );

        $object = $stmt->fetch();

        if ($object === false){

            return null;
        }

        return $object;
    }

    public function findByIds(array $ids): array
    {
        $this->adaptManager();

        return $this->manager->findBySame(['id' => $ids]);
    }

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): ?array
    {
        $this->adaptManager();

        if ($id === null) {
            $queryBuilder = $this->manager->getQueryBuilder();
            $queryBuilder->select($this->table)
                ->where($criteria, $operator);

            $stmt = $this->manager->setStmt(
                $queryBuilder->getQuery(),
                $criteria
            );
        }

        if ($id !== null) {
            $queryBuilder = $this->manager->getQueryBuilder();
            $queryBuilder->select($this->table)
                ->where($criteria, $operator)
                ->whereIn(['id0' => $id], 'id', 'NOT', 'AND');

            $stmt = $this->manager->setStmt(
                $queryBuilder->getQuery(),
                array_merge($criteria, ['id0' => $id])
            );
        }

        $array = $stmt->fetchAll();

        if ($array === []) {

            return null;
        }

        return $array;
    }

    public function create(HydratorInterface $hydrator): ?array
    {
        try {
            $this->manager->setTable($this->table);
            $this->manager->create($hydrator->serialize());

            return null;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return [];
    }

    public function createMultiple(array $fieldsSets): ?string
    {
        try {
            $this->manager->setTable($this->table);

            return $this->manager->createMultiple($fieldsSets);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function update(int $id, HydratorInterface $hydrator): ?array
    {
        try {
            $this->manager->setTable($this->table);
            $this->manager->update($hydrator->serialize(), ['id' => $id]);

            return null;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return [];
    }

    public function updateMultiple(array $fieldsSets): ?string
    {
        try {
            $this->manager->setTable($this->table);

            return $this->manager->updateMultiple($fieldsSets);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function remove(int $id): ?array
    {
        try {
            $this->manager->setTable($this->table);
            $this->manager->delete(['id' => $id]);

            return null;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return [];
    }
}