<?php

declare(strict_types=1);

namespace Paneric\PdoWrapper;

abstract class Repository
{
    protected $manager;
    protected $table;
    protected $dtoClass;
    protected $fetchMode;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function setPdoParams(array $params): void
    {
        $this->table = $params['table'];
        $this->dtoClass = $params['dto_class'];
        $this->fetchMode = $params['fetch_mode'];
    }

    public function adaptManager(): void
    {
        $this->manager->setTable($this->table);
        $this->manager->setDTOClass($this->dtoClass);
        $this->manager->setFetchMode($this->fetchMode);
    }

    public function getTable(): string
    {
        return $this->table;
    }

    public function findOneBy(array $criteria)//: false|[]|Object
    {
        $this->adaptManager();

        $queryResult = $this->manager->findOneBy($criteria);

        if ($queryResult === false) {
            return null;
        }

        return $queryResult;
    }

    public function findAll()// false|array
    {
        $this->adaptManager();

        return $this->manager->findBy([]);
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        $this->adaptManager();

        return $this->manager->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findBySame(array $criteriaSame, string $operator = 'OR')// false|array
    {
        $this->adaptManager();

        return $this->manager->findBySame($criteriaSame, $operator);
    }

    public function findByLike(array $criteriaLike, array $operators = ['OR'])// false|array
    {
        $this->adaptManager();

        return $this->manager->findByLike($criteriaLike, $operators);
    }

    public function getRowsNumber(): int
    {
        $this->adaptManager();

        return $this->manager->query('select count(*) from ' . $this->table)->fetchColumn();
    }
}
