<?php

declare(strict_types=1);

namespace Paneric\PdoWrapper;

class UserDTO
{
    private $id;
    private $ref;
    private $age;

    public function getId(): int
    {
        return $this->id;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function getAge(): int
    {
        return $this->age;
    }
}
