<?php

declare(strict_types=1);

namespace Paneric\PdoWrapper;

class UserRepository extends Repository
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'users';
        $this->dtoClass = UserDTO::class;
    }
}
